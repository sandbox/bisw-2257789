Change Content Status module is intended to provide admin to make a publish 
node to an non publish node and non publish node to publish node according to 
date range or content type or both. 

Setup

1. Install Change Content Status module
(unpacking it to your Drupal sites/all/modules directory if you're isntalling by 
hand).
2. Enable  Change Content Status  modules in Administer- > Site
building > Modules. To configure and setting Go to Administer-> Change Content
Status. Reference http://drupal.org/project/change_content_status
