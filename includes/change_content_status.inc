<?php

/**
 * @file
 * Select content type and date range to make any node publish
 * or non publish.
 */
 
/**
 * Implement change_content_status callback function.
 */
function change_content_status() {
  return drupal_get_form('change_content_status_form');
}

/**
 * Implement change_content_status_form().
 */
function change_content_status_form($form_state) {

  // Fetch all content type.
  $conten_type_result = db_query("SELECT * FROM {node_type} ");

  // Store all content type in an array.
  $conten_type_list = array();
  while ($rows = db_fetch_object($conten_type_result)) {
    $conten_type_list[$rows->type] = $rows->name;
  }

  // Define an array of publish and non publish for select options.
  $node_status = array(
    'publish' => 'Publish',
    'non publish' => 'Non Publish',
  );

  // Define the form.
  $forms = array();
  $forms['conten_type_list'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Type'),
    '#options' => $conten_type_list,
    '#description' => t('You can select multiple Content type'),
  );
  $forms['publish_date_from'] = array(
    '#type' => 'date',
    '#title' => t('From'),
    '#description' => t('Please provide From Date to make node publish or non publish'),
  );
  $forms['publish_date_to'] = array(
    '#type' => 'date',
    '#title' => t('To'),
    '#description' => t('Please provide To Date to make node non publish or publish'),
  );
  $forms['node_status'] = array(
    '#type' => 'radios',
    '#title' => t('Select Node Status'),
    '#options' => $node_status,
    '#description' => t('Select Publish to make node publish or select Non publish to make node non publish'),
  );
  $forms['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update',
  );
  return $forms;
}

/**
 * Implement submit function for the form.
 */
function change_content_status_form_submit($form, &$form_state) {

  // Get the value from 'publish_date_from'.
  $from_day    = $form_state['values']['publish_date_from']['day'];
  $from_month  = $form_state['values']['publish_date_from']['month'];
  $from_year   = $form_state['values']['publish_date_from']['year'];
  $from_date   = strtotime($from_year . '-' . $from_month . '-' . $from_day);

  // Get the value from 'publish_date_to'.
  $to_day      = $form_state['values']['publish_date_to']['day'];
  $to_month    = $form_state['values']['publish_date_to']['month'];
  $to_year     = $form_state['values']['publish_date_to']['year'];
  $to_date     = strtotime($to_year . '-' . $to_month . '-' . $to_day);

  $node_status = $form_state['values']['node_status'];
  $conten_type = $form_state['values']['conten_type_list'];

  // Set all the selected content type in an array.
  $final_type  = array();
  foreach ($conten_type as $type_val) {
    // Filter the content type if selected any.
    if ($type_val != '0') {
      $final_type[] = $type_val;
    }
  }

  // Define a variable to count all the selected content type.
  $count_type = 0;
  $count_type = count($final_type);

  // Set a integer variable to 0 and use this to check first content type.
  $count_no = 0;
  $status = 2;

  // Set the value for a part of where condition to select fields from node.
  foreach ($final_type as $final_val) {
    if ($count_no == 0) {
      if ($count_type == 1) {
        $output = "&& type = '$final_val'";
      }
      else {
        $output .= "&&( type = '$final_val'";
      }
    }
    else {
      $output .= " || type = '$final_val'";
    }
    $count_no++;
  }
  if ($count_type > 1 && $count_no > 1) {
    $output .= ")";
  }

  // Get all the nodes according to given date range and content type.
  $result = db_query("SELECT * FROM {node} WHERE created >= '%s' AND created <= '%s' $output", $from_date, $to_date);

  // Set status value.
  ($node_status == 'publish') ? $status = 1 : $status = 0;
  if($status < 2) {
    while ($row_result = db_fetch_object($result)) {
      // Set node as non publish.
      db_query("UPDATE {node} SET status = '%d' WHERE nid = '%s'", $status, $row_result->nid);
    }
  }	
  $count_result = db_affected_rows($result);
  // Show message if any node effected by given data.
  if ($count_result > 0) {
    drupal_set_message("Node are successfully updated");
  }
  else {
    // Show message if no node effected by given data.
    drupal_set_message("There are no Node with this value");
  }
}
